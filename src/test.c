/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define MAX_LINES_CHAR 256
#define NAME_CHARSIZE MAX_LINES_CHAR
/*----------------------------------------------------------------------------*/
#include "type.h"
#include "list.h"
#include "cstr.h"
/*----------------------------------------------------------------------------*/
typedef struct _text_t {
	char cstr[NAME_CHARSIZE];
	int id, tpad;
} text_t;
/*----------------------------------------------------------------------------*/
my1item_t* make_text_item(void) {
	void *text;
	my1item_t* item;
	item = make_item();
	if (item) {
		text = malloc(sizeof(text_t));
		if (text) item->data = text;
		else {
			free((void*)item);
			item = 0x0;
		}
	}
	return item;
}
/*----------------------------------------------------------------------------*/
void free_text_item(void* that) {
	my1item_t* item = (my1item_t*)that;
	free(item->data);
	free(that);
}
/*----------------------------------------------------------------------------*/
int print_item(void* item) {
	text_t* text = (text_t*) item;
	printf("ID: %010d , Label: %s\n",text->id,text->cstr);
	return 0; /* preserve item */
}
/*----------------------------------------------------------------------------*/
void* find_item_id(my1list_t* list, int itemid) {
	void* find = 0x0;
	list->curr = 0x0;
	while (list_scan_item(list)) {
		text_t *test = (text_t*) list->curr->data;
		if (test->id==itemid) {
			find = (void*)test;
			break;
		}
	}
	return find;
}
/*----------------------------------------------------------------------------*/
int find_free_id(my1list_t* list) {
	int max_id = 0;
	list->curr = 0x0;
	while (list_scan_item(list)) {
		text_t *test = (text_t*) list->curr->data;
		if (test->id>max_id)
			max_id = test->id;
	}
	return ++max_id;
}
/*----------------------------------------------------------------------------*/
int compare_label(void* pchk, void* qchk) {
	text_t *pdat = (text_t*) pchk;
	text_t *qdat = (text_t*) qchk;
	return strncmp(pdat->cstr,qdat->cstr,NAME_CHARSIZE);
}
/*----------------------------------------------------------------------------*/
void print_head(my1list_t* list) {
	printf("\n");
	printf("----------------------------\n");
	printf("Test Program for my1codebase\n");
	printf("----------------------------\n");
	printf("{Init:%p",list->init);
	printf(",Last:%p}",list->last);
	printf("[Size:%d]\n",list->size);
}
/*----------------------------------------------------------------------------*/
typedef struct _data_t {
	int test, loop;
	text_t* text;
	my1item_t* item;
	my1list_t list;
	my1cstr_t buff, copy, cmd0, temp, args;
} data_t;
/*----------------------------------------------------------------------------*/
void data_init(data_t* data) {
	list_init(&data->list,free_text_item);
	cstr_init(&data->buff);
	cstr_init(&data->copy);
	cstr_init(&data->cmd0);
	cstr_init(&data->temp);
	cstr_init(&data->args);
	cstr_resize(&data->buff,NAME_CHARSIZE);
}
/*----------------------------------------------------------------------------*/
void data_free(data_t* data) {
	cstr_free(&data->args);
	cstr_free(&data->temp);
	cstr_free(&data->cmd0);
	cstr_free(&data->copy);
	cstr_free(&data->buff);
	list_free(&data->list);
}
/*----------------------------------------------------------------------------*/
#include "cmds.h"
/*----------------------------------------------------------------------------*/
void do_info(my1cmd_t* this, data_t* data) {
	print_head(&data->list);
}
/*----------------------------------------------------------------------------*/
void do_list(my1cmd_t* this, data_t* data) {
	printf("\n");
	if (data->list.size>0) {
		printf("----------\n");
		printf("Items List\n");
		printf("----------\n");
		data->list.curr = 0x0;
		while (list_scan_item(&data->list))
			print_item(data->list.curr->data);
	}
	else printf("@@ No item in list!\n");
}
/*----------------------------------------------------------------------------*/
void do_push(my1cmd_t* this, data_t* data) {
	printf("\n");
	cstr_shword(&data->buff,&data->args,COMMON_DELIM);
	if (!data->args.fill) {
		fprintf(stderr,"** No label given! {%s}\n",data->copy.buff);
		return;
	}
	if (data->buff.fill)
		fprintf(stderr,"** Extra param! {%s}\n",data->buff.buff);
	data->item = make_text_item();
	if (!data->item) {
		fprintf(stderr,"** Cannot create item!\n");
		return;
	}
	data->text = (text_t*) data->item->data;
	data->text->id = find_free_id(&data->list);
	strncpy(data->text->cstr,data->args.buff,NAME_CHARSIZE);
	data->text->cstr[NAME_CHARSIZE-1] = 0x0; /* just in case */
	list_push_item(&data->list,data->item);
	printf("Item '%s' pushed! (%d)\n",data->text->cstr,data->list.size);
}
/*----------------------------------------------------------------------------*/
void do_pull(my1cmd_t* this, data_t* data) {
	printf("\n");
	if (data->buff.fill)
		fprintf(stderr,"** Extra param! {%s}\n",data->buff.buff);
	if (data->list.size>0) {
		data->text = (text_t*) data->list.init->data;
		printf("Item '%s' pulled!\n",data->text->cstr);
		list_pull_item(&data->list);
	}
	else printf("** No items in list!\n");
}
/*----------------------------------------------------------------------------*/
void do_remove(my1cmd_t* this, data_t* data) {
	printf("\n");
	cstr_shword(&data->buff,&data->args,COMMON_DELIM);
	if (!data->args.fill) {
		fprintf(stderr,"** No label given! {%s}\n",data->copy.buff);
		return;
	}
	if (data->buff.fill)
		fprintf(stderr,"** Extra param! {%s}\n",data->buff.buff);
	data->item = 0x0;
	data->list.curr = 0x0;
	while (list_scan_item(&data->list)) {
		data->text = (text_t*) data->list.curr->data;
		if (!strcmp(data->args.buff,data->text->cstr)) {
			data->item = data->list.curr;
			break;
		}
	}
	if (data->item) {
		printf("Item '%s' removed!\n",data->args.buff);
		list_hack_item(&data->list,data->item);
	}
	else printf("** Item '%s' not found!\n",data->args.buff);
}
/*----------------------------------------------------------------------------*/
void do_sort(my1cmd_t* this, data_t* data) {
	printf("\n");
	if (data->buff.fill)
		fprintf(stderr,"** Extra param! {%s}\n",data->buff.buff);
	printf("Sorting list... ");
	list_sort(&data->list,&compare_label);
	printf("done!\n");
}
/*----------------------------------------------------------------------------*/
void do_empty(my1cmd_t* this, data_t* data) {
	printf("\n");
	if (data->buff.fill)
		fprintf(stderr,"** Extra param! {%s}\n",data->buff.buff);
	printf("Emptying list... ");
	list_free(&data->list);
	printf("done!\n");
}
/*----------------------------------------------------------------------------*/
void do_help(my1cmd_t* this, data_t* data) {
	printf("\n-- Command list:\n\n");
	while (this->scmp>0) {
		printf("   %s - %s\n",this->name,this->info);
		this++;
	}
}
/*----------------------------------------------------------------------------*/
void cmds_create(my1cmds_t* that) {
	cmds_make(that,"help","Show this help",CMDFLAG_TASK,(ptask_t)do_help);
	cmds_make(that,"quit","Exit program",CMDFLAG_EXIT,0x0);
	cmds_make(that,"exit","",CMDFLAG_EXIT,0x0);
	cmds_make(that,"info","Print list info",CMDFLAG_TASK,(ptask_t)do_info);
	cmds_make(that,"list","Show list",CMDFLAG_TASK,(ptask_t)do_list);
	cmds_make(that,"push","Push new item into list",
		CMDFLAG_TASK,(ptask_t)do_push);
	cmds_make(that,"pull","Pull item from list",CMDFLAG_TASK,(ptask_t)do_pull);
	cmds_make(that,"remove","Remove specific item from list",
		CMDFLAG_TASK,(ptask_t)do_remove);
	cmds_make(that,"sort","Sort items in list",CMDFLAG_TASK,(ptask_t)do_sort);
	cmds_make(that,"empty","Empty list",CMDFLAG_TASK,(ptask_t)do_empty);
	//cmds_make(that,"","",0,0x0))
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1cmds_t cmds;
	data_t data;
	my1cmd_t* pcmd;
	cmds_init(&cmds);
	data_init(&data);
	do_info(0x0,&data);
	cmds.data = (void*)&data;
	cmds_create(&cmds);
	do {
		printf("\n> ");
		data.test = cstr_read_line(&data.buff,stdin);
		if (!data.test) {
			do_help(cmds.list,&data);
			continue;
		}
		cstr_trimws(&data.buff,1);
		cstr_copy(&data.copy,&data.buff);
		cstr_null(&data.cmd0); cstr_null(&data.args);
		cstr_shword(&data.buff,&data.cmd0,COMMON_DELIM);
		data.loop=0; pcmd=cmds.list;
		do {
			if (!strncmp(data.cmd0.buff,pcmd->name,pcmd->scmp)) {
				if (pcmd->exec) pcmd->exec(pcmd,&data);
				break;
			}
			data.loop++; pcmd++;
			if (!pcmd->scmp) {
				printf("\n** Invalid command '%s'!\n",data.cmd0.buff);
				break;
			}
		} while (1);
		if (pcmd->flag&CMDFLAG_EXIT) {
			printf("\n@@ Done!\n\n");
			break;
		}
	} while(1);
	data_free(&data);
	cmds_free(&cmds);
	return 0;
}
/*----------------------------------------------------------------------------*/
