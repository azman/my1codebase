/*----------------------------------------------------------------------------*/
#include "list.h"
#include "path.h"
/*----------------------------------------------------------------------------*/
void path_print_info(my1path_t* path, int full) {
	if (!path->path) return;
	printf("  %s ",path->perm);
	if (full)
		printf("%s {path:%s,name:%s}\n",path->full,path->path,path->name);
	else {
		printf("%s",path->name);
		if (path->real)
			printf(" -> %s",path->real);
		if (path->flag&PATH_FLAG_PATH)
			putchar(PATH_SEPARATOR_CHAR);
		printf("\n");
	}
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1path_t test, next, that, prog;
	my1list_t list;
	char *pick = argc>1?argv[1] : ".";
	/* check running progam */
	path_init(&prog);
	path_access(&prog,argv[0]);
	if (!prog.trap) {
		printf("DoExec: ");
		path_print_info(&prog,1);
	}
	/* check name given */
	path_init(&test);
	path_access(&test,pick);
	if (test.trap) printf("Error accessing '%s' (%d)\n",pick,test.trap);
	else {
		/* check parent */
		path_init(&next);
		path_parent(&test,&next);
		if (next.trap&PATH_TRAP_ERROR)
			printf("Error getting parent for '%s' (%x)\n",test.name,next.trap);
		else {
			printf("Parent: ");
			path_print_info(&next,1);
		}
		printf("Access: ");
		path_print_info(&test,1);
		if (test.flag&PATH_FLAG_PATH) {
			/* check paths */
			printf("DoList:\n");
			list_init(&list,list_free_item_data);
			path_dolist(&test,&list,PATH_LIST_BOTH);
			list.curr = 0x0;
			while (list_scan_item(&list)) {
				pick = (char*) list.curr->data;
				path_init(&that);
				path_access(&that,pick);
				if (that.name)
					path_print_info(&that,0);
				path_free(&that);
			}
			if (!list.size) printf("  Empty path!\n");
			list_free(&list);
		}
		/* free parent */
		path_free(&next);
	}
	/* cleanup */
	path_free(&test);
	path_free(&prog);
	/* show env PATH */
	list_init(&list,list_free_item_data);
	pick = path_getenv(&list);
	if (pick) {
		printf("PATH=%s\nList:\n",pick);
		list.curr = 0x0;
		while (list_scan_item(&list)) {
			pick = (char*) list.curr->data;
			printf("--> %s\n",pick);
		}
		printf("Done.\n");
	}
	list_free(&list);
	return 0;
}
/*----------------------------------------------------------------------------*/
