/*----------------------------------------------------------------------------*/
#include "my1bytes.h"
/*----------------------------------------------------------------------------*/
typedef my1bytes_t heap_t;
/*----------------------------------------------------------------------------*/
#define HEAP(ph) ((heap_t*)ph)
#define heap_init(ph) bytes_init(HEAP(ph))
#define heap_free(ph) bytes_free(HEAP(ph))
#define heap_make(ph,sz) bytes_make(HEAP(ph),sz)
#define heap_fill(ph,b8) bytes_fill(HEAP(ph),b8)
#define heap_free(ph,b8) bytes_slip(HEAP(ph),b8)
#define heap_free(ph,pb,sz) bytes_more(HEAP(ph),((byte08_t*)pb),sz)
#define heap_free(ph,b8,sz) bytes_fill_size(HEAP(ph),b8,sz)
/*----------------------------------------------------------------------------*/
#ifndef __MY1_HEADER_ONLY__
/*----------------------------------------------------------------------------*/
#ifndef __MY1BYTES_C__
#define __MY1BYTES_C__
/*----------------------------------------------------------------------------*/
#include "my1bytes.c"
/*----------------------------------------------------------------------------*/
#endif /** __MY1BYTES_C__ */
/*----------------------------------------------------------------------------*/
#endif /** __MY1_HEADER_ONLY__ */
/*----------------------------------------------------------------------------*/
