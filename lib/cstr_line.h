/*----------------------------------------------------------------------------*/
#include "cstr_core.h"
#include "my1cstr_line.h"
/*----------------------------------------------------------------------------*/
#ifndef __MY1_HEADER_ONLY__
/*----------------------------------------------------------------------------*/
#ifndef __MY1CSTR_LINE_C__
#define __MY1CSTR_LINE_C__
/*----------------------------------------------------------------------------*/
#include "my1cstr_line.c"
/*----------------------------------------------------------------------------*/
#endif /** __MY1CSTR_LINE_C__ */
/*----------------------------------------------------------------------------*/
#endif /** __MY1_HEADER_ONLY__ */
/*----------------------------------------------------------------------------*/
