# makefile for my1codebase - now dependent on my1codelib

# look for all c codes in src and current path
MODULES = $(subst .c,,$(subst src/,,$(sort $(wildcard src/*.c))))
MODULES += $(subst .c,,$(sort $(wildcard *.c)))

LIBPATH = ../my1codelib/src

# build stuffs
CC = gcc -c
LD = gcc
DELETE = rm -rf
# for static compile: -static --static
CFLAGS += -Wall -I./src -I./lib -I$(LIBPATH)
ifneq ($(DOFLAG),)
	CFLAGS += $(DOFLAG)
endif
LFLAGS += $(LDFLAGS)
ifneq ($(DOLINK),)
	LFLAGS += $(DOLINK)
endif
ifeq ($(DOSTATIC),1)
	CFLAGS += -static --static
endif

info:
	@echo
	@echo "Run 'make <mod>'"
	@echo
	@echo "  <mod> = { $(MODULES) }"
	@echo
	@echo "To add compiler switch (e.g. -Wall), do 'make <app> DOFLAG=-Wall'"
	@echo "To link a library (e.g. math), do 'make <app> DOLINK=-lm'"
	@echo

all: $(MODULES)

%: src/%.c src/%.h
	$(LD) $(CFLAGS) -o $@ $< $(LFLAGS)

%: src/%.c
	$(LD) $(CFLAGS) -o $@ $< $(LFLAGS)

%: %.c
	$(LD) $(CFLAGS) -o $@ $< $(LFLAGS)

clean:
	-$(DELETE) $(MODULES) *.o

sweep:
	-@$(DELETE) $(MODULES) *.o
